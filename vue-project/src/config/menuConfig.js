export default [
    {
      name: 'tabels',
      path: '/tabels',
      icon: 'el-icon-location',
      title:'表格',
      children: [
        {
          title:'表格一',
          name: 'tabel',
          path: '/tabels/tabel',
          icon: 'el-icon-s-data',
          src:'/tabels/tabel',
          
        },
        {
          title:'表格二',
          name: 'tabel2',
          path: '/tabels/tabel2',
          icon: 'el-icon-s-data',
          src:'/tabels/tabel2',
          
        },
        {
          title:'可编辑表格',
          name: 'formData',
          path: '/tabels/formData',
          icon: 'el-icon-s-data',
          src:'/tabels/formData',
          
        },
      ]
    },

    {
      name: 'form',
      path: '/form',
      icon: 'el-icon-s-grid',
      title:'表单',
      children: [
        {
          title:'日期',
          name: 'datetime',
          path: '/form/datetime',
          icon: 'el-icon-date',
          src:'/form/datetime',
        }
      ]
    }
  ]
  