import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home'
import index from '@/views/index'

// import $store  from '@/store'
import * as $T from '@/utils/tools'


Vue.use(VueRouter)

export  const IndexRoute = {
  path: '/',
  name: 'Home',
  component: Home,
  redirect: '/index',
  children: [{
    path: '/index',
    name: 'index',
   component: index,
    title:'首页',
  }]
}


const routerContext = require.context('./', true, /index\.js$/)
routerContext.keys().forEach(route => {
  // 如果是根目录的 index.js 、不处理
  if (route.startsWith('./index')) {
    return
  }
  const routerModule = routerContext(route)
  /**
   * 兼容 import export 和 require module.export 两种规范
   */
  IndexRoute.children = [
    ...IndexRoute.children,
    ...(routerModule.default || routerModule)
  ]
  // sss = [
  //   ...(routerModule.default || routerModule)
  // ]
})
let routes = [
  IndexRoute,
  {
    path: '*',
    redirect: '/index'
  }
]

const router = new VueRouter({
  mode: 'history',
  // base: 'vef',
  routes: routes
})
router.beforeEach((to, from, next) => {
//   // 如果存在用户信息
//   if ($store.state.app.user) {
    $T.toDefaultPage([...IndexRoute.children], to.name, router, next)
//   } else {
//     // 获取用户信息
//     $http.get($API.PUBLIC_GET_USER_INFO).then(response => {
//       // 保存用户信息去store
//       $T.setUserInfo(response.data)
//       // $store.state.app.user = response.data
//       $T.toDefaultPage([...appRouter.children, ...otherRouter.children], to.name, router, next)
//     }).catch(() => {
//       $T.error('获取用户信息失败！')
//     })
//   }
//   // }
})

router.beforeResolve((routeTo, routeFrom, next) => {
  next()
})

router.afterEach((to) => {
  $T.openNewPage(router.app, to.name)

})

export default router
