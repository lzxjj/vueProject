
export default [
    {
        path: '/form/datetime',
        name: 'datetime',
        title:'日期',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '@/views/form/datetime.vue')
      },


]