
export default [
    {
        path: '/tabels/tabel',
        name: 'tabel',
        title:'表格一',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '@/views/tabels/tabel.vue')
      },
      {
        path: '/tabels/tabel2',
        name: 'tabel2',
        title:'表格二',
        component: () => import(/* webpackChunkName: "about" */ '@/views/tabels/tabel2.vue')
      },
      {
        path: '/tabels/formData',
        name: 'formData',
        title:'可编辑表格',
        component: () => import(/* webpackChunkName: "about" */ '@/views/tabels/formData.vue')
      },

]