// components/index.js
import Vue from 'vue'

// 自动加载 global 目录下的 .js 或vue 结尾的文件
const componentsContext = require.context('./global', true, /\.(vue|js)$/)

componentsContext.keys().forEach(component => {
  const componentConfig = componentsContext(component)
  /**
  * 兼容 import export 和 require module.export 两种规范
  */
  const ctrl = componentConfig.default || componentConfig
  Vue.component(ctrl.name, ctrl)
})


// import Vue from 'vue'
// import upperFirst from 'lodash/upperFirst'     // 首字母大小写
// import camelCase from 'lodash/camelCase'   // 驼峰命名

// const requireComponent = require.context(
//   // components 文件夹的相对路径
//   './base',
//   // 是否查找子文件夹
//   false,
//   // 用于匹配组件文件名的正则表达式
//   /Base[A-Z]\w+\.(vue|js)$/
// )

// requireComponent.keys().forEach(fileName => {
//   // 获取组件配置
//   const componentConfig = requireComponent(fileName)
//   // 取得组件的 Pascal 式命名
//   const componentName = upperFirst(
//     camelCase(
//       // 将文件名前面的 `'./` 和扩展名剥离
//       fileName.replace(/^\.\/(.*)\.\w+$/, '$1')
//     )
//   )
//   // 如果组件是通过 `export default` 导出，
//   // 则在 `.default` 中，查找组件选项，
//   // 否则回退至模块根对象中，查找组件选项
//   Vue.component(componentName, componentConfig.default || componentConfig)
// })