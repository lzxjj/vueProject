import menuConfig from '@/config/menuConfig'
import * as T from '@/utils/tools'

export default {
    name: 'siders',
    data () {
      return {
        menuConfig,
        defaultActive: '',
        isCollapse: false,
      }
    },
    computed: {
    //   ...mapGetters(['getMenuCollapse', 'getMenuStyle', 'getCurrentMenuStyle'])
    },
    created () {
      this.defaultActive =true

    },
    methods: {
      flagcollapse() {
        this.isCollapse = !this.isCollapse;
      },
      handleAddTab(item){
        console.log(item)
        if (item.src) {
          // 有iframe外链接的场景
          T.openNewPage(this, item.name)
        } else {
          this.$router.push({
            name: item.name
          })
        }
      },
      renderMenu (menus) {
        return menus.map(menu => {
          if (menu.children) {
            return (
             <el-submenu index={menu.path}>
                <template slot="title">
                <i class={menu.icon}></i>
                  <span>{menu.title}</span>
                </template>
                {this.renderMenu(menu.children)}
              </el-submenu>
            )
          } else {
            return (
             <el-menu-item index={menu.path}  click={this.handleAddTab(menu)}>
                <i class={menu.icon}></i>
                <span slot="title">{menu.title}</span>
              </el-menu-item>
            )
          }
        })
      }
    },
  
    render () {
      return (
        <div class="base-nav-menu-wrap">
        <el-aside class="left_nav" style="width: auto;">
          <div class="sider-logo">
            <i
              class={[this.isCollapse ? 'el-icon-s-unfold' : 'el-icon-s-fold',"collapse"]}
              on-click={this.flagcollapse}
            ></i>
          </div>
          <el-menu
            class="el-menu-vertical-demo"
            style="border:none"
            collapse={this.isCollapse}
            text-color="#fff"
            background-color="#545c64"
            active-text-color="#ffd04b"
            router={true}
          >
             {this.renderMenu(this.menuConfig)}
          </el-menu>
        </el-aside>
      </div>
      )
    }
}