export function openNewPage(vm, name, params, query) {
  if(!vm.$store){
    return
  }
    let openedPagesArr = vm.$store.state.app.openedPages
    let tagHasOpened = false
  
    for (let item of openedPagesArr) {
      if (name === item.name) {
        tagHasOpened = true
        break
      }
    }
  
    if (!tagHasOpened) {
      let tagArr = vm.$store.state.app.tagsList.filter((item) => {
        return name === item.name
      })
      if (tagArr.length) {
        let tagObj = tagArr[0]
        vm.$store.commit('addPageTag', {
          title: tagObj.title,
          path: tagObj.path ? tagObj.path : '',
          name: tagObj.name,
          src: tagObj.src ? tagObj.src : ''
        })
      }
    }
  
    // 当前打开tab的name
    vm.$store.state.app.currentTagName = name
  }
  
  export function toDefaultPage(routers, name, route, next) {
    let len = routers.length
    let i = 0
    let notHandle = true
    while (i < len) {
      if (routers[i].name === name && routers[i].children && routers[i].redirect === undefined) {
        route.replace({
          name: routers[i].children[0].name
        })
        notHandle = false
        next()
        break
      }
      i++
    }
    if (notHandle) {
      next()
    }
  }

  export function handleCofirm(vim,text = '确定执行此操作吗？', type = 'warning') {
    return  vim.$confirm(text, '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: type,
      center: true
    })
  }
  