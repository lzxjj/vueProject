import { IndexRoute } from '@/router/index'

const state = {
  createId:'',
  stationId:'',
  isPlatform:false,
  parentId:'',
  gxsqOrgcode:'',
  openedPages: [
    {
    title: '首页',
    path:'/index',
    name: 'index',
    src: ''
  }
],
  currentTagName: 'index', // iframe就是name,路由则是路由名字
  tagsList: [...IndexRoute.children],
  user: 'user',
  menuFunc:null,
  orgcode:null,
  avator: 'https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b400f67d360ea1cea8febaf5d&src=http://img3.redocn.com/tupian/20141217/zhongguoshihuabiaozhi_3709483.jpg'
}

const mutations = {
  addIframToTagsList(state, iframsArr) {
    iframsArr.forEach((obj) => {
      state.tagsList.push({
        src: obj.src,
        path: '',
        name: obj.name,
        title: obj.title,
        component: ''
      })
    })
  },
  addPageTag(state, tagObj) {
    // if (!Util.oneOf(tagObj.name, state.dontCache)) {
    //   state.cachePage.push(tagObj.name);
    //   localStorage.cachePage = JSON.stringify(state.cachePage);
    // }
    state.openedPages.push(tagObj)
    // localStorage.pageOpenedList = JSON.stringify(state.pageOpenedList)
  },
  removePageTag(state, tagNamePos) {
    state.openedPages.splice(tagNamePos, 1)
  },
  clearAllTags(state) {
    state.openedPages.splice(1)
  },
  // 设置当前激活的tab
  set_active_index(state, index) {
    state.currentTagName = index
  }
}

export default {
  state,
  mutations
}
